const checkRole = (roles) => {
    return (req, res, next) => {
        const user = req.user;

        if (!user || !roles.includes(user.role)) {
            return res.status(403).json({ message: 'Access Forbidden' });
        }

        next();
    };
};

module.exports = {
    checkRole,
};
