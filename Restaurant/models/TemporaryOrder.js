// models/TemporaryOrder.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const Table = require('./Table');
const MenuItem = require('./MenuItem');

const TemporaryOrder = sequelize.define('TemporaryOrder', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    table_number: {
        type: DataTypes.INTEGER,
    },
    customer_identifier: {
        type: DataTypes.STRING,
    },
    total_price: {
        type: DataTypes.DECIMAL(10, 2),
    },
    status: {
        type: DataTypes.ENUM('pending', 'temporary'),
        defaultValue: 'temporary',
    },
    order_time: {
        type: DataTypes.DATE,
    },
    menu_item_id: {
        type: DataTypes.INTEGER,
    },
    quantity: {
        type: DataTypes.INTEGER,
    },
    remark: {
        type: DataTypes.TEXT,
    },
});

module.exports = TemporaryOrder;
