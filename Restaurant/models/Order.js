// models/Order.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Order = sequelize.define('Order', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    table_number: {
        type: DataTypes.INTEGER,
    },
    customer_identifier: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    total_price: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false,
    },
    status: {
        type: DataTypes.ENUM('pending', 'in progress', 'completed', 'payed', 'confirmed', 'ready to serve'),
        defaultValue: 'pending',
    },
    order_time: {
        type: DataTypes.DATE,
    },
    menu_item_id: {
        type: DataTypes.INTEGER,
    },
    quantity: {
        type: DataTypes.INTEGER,
    },
    remark: {
        type: DataTypes.TEXT,
    },
});


module.exports = Order;
