// models/Transaction.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Transaction = sequelize.define('Transaction', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    type: {
        type: DataTypes.ENUM('payment', 'refund', 'expense'),
    },
    amount: {
        type: DataTypes.DECIMAL(10, 2),
    },
    timestamp: {
        type: DataTypes.DATE,
    },
    notes: {
        type: DataTypes.TEXT,
    },
    customer_identifier: {
        type: DataTypes.INTEGER,
    },
});

module.exports = Transaction;
