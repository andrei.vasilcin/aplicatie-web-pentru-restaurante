// models/Table.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const Reservation = require('./Reservation');

const Table = sequelize.define('Table', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    table_number: {
        type: DataTypes.INTEGER,
    },
    capacity: {
        type: DataTypes.INTEGER,
    },
    status: {
        type: DataTypes.ENUM('available', 'reserved', 'occupied'),
        defaultValue: 'available',
    },
});

module.exports = Table;
