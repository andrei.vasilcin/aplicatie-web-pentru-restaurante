// models/Category.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Category = sequelize.define('Category', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: DataTypes.STRING,
        foreignKey: true,
        allowNull: false,
    },
    description: {
        type: DataTypes.TEXT,
    },
});
// Category.hasMany(MenuItem, { foreignKey: 'category', sourceKey: 'name' });
module.exports = Category;
