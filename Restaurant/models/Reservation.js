// models/Reservation.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const Table = require('./Table');

const Reservation = sequelize.define('Reservation', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    customer_name: {
        type: DataTypes.STRING,
    },
    reservation_time: {
        type: DataTypes.DATE,
    },
    status: {
        type: DataTypes.ENUM('confirmed', 'pending', 'canceled'),
        defaultValue: 'pending',
    },
});

module.exports = Reservation;
