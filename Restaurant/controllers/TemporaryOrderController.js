// module.exports = TemporaryOrderController;
const { DataTypes } = require('sequelize');
const Table = require('../models/Table');
const MenuItem = require('../models/MenuItem');
const MenuItemIngredient = require('../models/MenuItemIngredient');
const Ingredient = require('../models/Ingredient');

const { io } = require('../app');

module.exports = sequelize => {
    const TemporaryOrder = sequelize.define('TemporaryOrder', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        table_number: {
            type: DataTypes.INTEGER,
        },
        customer_identifier: {
            type: DataTypes.STRING,
        },
        total_price: {
            type: DataTypes.DECIMAL(10, 2),
        },
        status: {
            type: DataTypes.ENUM('pending', 'temporary'),
            defaultValue: 'temporary',
        },
        order_time: {
            type: DataTypes.DATE,
        },
        menu_item_id: {
            type: DataTypes.INTEGER,
        },
        quantity: {
            type: DataTypes.INTEGER,
        },
        remark: {
            type: DataTypes.TEXT,
        },
    });
    TemporaryOrder.belongsTo(Table, { foreignKey: 'table_number' });
    TemporaryOrder.belongsTo(MenuItem, { foreignKey: 'menu_item_id', as: 'menuItem' });
    return {
        // get all temporary orders
        getAllTemporaryOrders: async (req, res) => {
            try {
                const temporaryOrders = await TemporaryOrder.findAll({
                    include: {
                        model: MenuItem,
                        as: 'menuItem',
                        attributes: ['image_url', 'name']
                    },
                });

                res.json(temporaryOrders);
            } catch (error) {
                console.error('Error fetching temporary orders:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get temporary order by ID
        getTemporaryOrderById: async (req, res) => {
            const temporaryOrderId = req.params.id;
            try {
                const temporaryOrder = await TemporaryOrder.findByPk(temporaryOrderId);
                if (!temporaryOrder) {
                    res.status(404).json({ message: 'Temporary order not found' });
                } else {
                    res.json(temporaryOrder);
                }
            } catch (error) {
                console.error('Error fetching temporary order:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
        // create a new temporary order waiter side
        createTemporaryOrderWaiter: async (req, res) => {
            const { table_number, customer_identifier, total_price, status, order_time, menu_item_id, quantity, remark } = req.body;
            try {
                const newTemporaryOrder = await TemporaryOrder.create({
                    table_number,
                    customer_identifier,
                    total_price,
                    status,
                    order_time,
                    menu_item_id,
                    quantity,
                    remark,
                });
                console.log("starting emitting for: ", newTemporaryOrder);
                req.io.emit('temporaryOrdersCreated', newTemporaryOrder);
                res.status(201).json(newTemporaryOrder);
            } catch (error) {
                console.error('Error creating temporary order:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new temporary order
        createTemporaryOrder: async (req, res) => {
            const { table_number, customer_identifier, total_price, status, order_time, menu_item_id, quantity, remark } = req.body;

            try {
                const menuItemIngredients = await MenuItemIngredient.findAll({
                    where: { menu_item_id },
                    include: Ingredient
                });

                for (const item of menuItemIngredients) {
                    const ingredient = item.Ingredient;
                    if (item.quantity_required * quantity > ingredient.quantity) {
                        return res.status(400).json({ message: `Not enough ${ingredient.name} available.` });
                    }
                }
                for (const item of menuItemIngredients) {
                    const ingredient = item.Ingredient;
                    ingredient.quantity -= item.quantity_required * quantity;
                    await ingredient.save();
                }

                const newTemporaryOrder = await TemporaryOrder.create({
                    table_number,
                    customer_identifier,
                    total_price,
                    status,
                    order_time,
                    menu_item_id,
                    quantity,
                    remark,
                });
                console.log("starting emitting for: ", newTemporaryOrder);
                req.io.emit('temporaryOrdersCreated', newTemporaryOrder);
                res.status(201).json(newTemporaryOrder);

            } catch (error) {
                console.error('Error creating temporary order:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update temporary order by ID
        updateTemporaryOrder: async (req, res) => {
            const temporaryOrderId = req.params.id;
            const { table_number, customer_identifier, total_price, status, order_time, menu_item_id, quantity, remark } = req.body;

            try {
                const temporaryOrder = await TemporaryOrder.findByPk(temporaryOrderId);
                if (!temporaryOrder) {
                    return res.status(404).json({ message: 'Temporary order not found' });
                }

                const oldQuantity = temporaryOrder.quantity;
                console.log(`Old Quantity: ${oldQuantity}`);
                console.log(`New Quantity: ${quantity}`);

                const menuItemIngredients = await MenuItemIngredient.findAll({
                    where: { menu_item_id: temporaryOrder.menu_item_id },
                    include: Ingredient
                });

                // if the quantity has increased
                if (oldQuantity < quantity) {
                    console.log("Quantity has increased");
                    for (const item of menuItemIngredients) {
                        const ingredient = item.Ingredient;
                        const requiredQuantity = item.quantity_required * (quantity - oldQuantity);
                        console.log(`Ingredient Name: ${ingredient.name}, Required Quantity: ${requiredQuantity}, Available Quantity: ${ingredient.quantity}`);
                        if (requiredQuantity > ingredient.quantity) {
                            return res.status(400).json({ message: `Not enough ${ingredient.name} available.` });
                        }
                        ingredient.quantity -= requiredQuantity;
                        console.log(`Updated Quantity (Increased): ${ingredient.quantity}`);
                        await ingredient.save();
                    }
                }

                // if the quantity has decreased
                if (oldQuantity > quantity) {
                    console.log("Quantity has decreased");
                    for (const item of menuItemIngredients) {
                        const ingredient = item.Ingredient;
                        const requiredQuantity = parseFloat((item.quantity_required * (oldQuantity - quantity)).toFixed(2));
                        console.log(`Ingredient Name: ${ingredient.name}, Required Quantity: ${requiredQuantity}, Available Quantity: ${ingredient.quantity}`);
                        const updatedQuantity = parseFloat(ingredient.quantity) + requiredQuantity;
                        ingredient.quantity = parseFloat(updatedQuantity.toFixed(2)); // restock exact amount
                        console.log(`Updated Quantity (Decreased): ${ingredient.quantity}`);
                        await ingredient.save();
                    }
                }



                // update the temporary order
                await temporaryOrder.update({
                    table_number,
                    customer_identifier,
                    total_price,
                    status,
                    order_time,
                    menu_item_id,
                    quantity,
                    remark,
                });
                res.json(temporaryOrder);
            } catch (error) {
                console.error('Error updating temporary order:', error);
                res.status(500).json({ message: 'Internal server error' });
            }

        },



        // delete temporary order by ID
        deleteTemporaryOrder: async (req, res) => {
            const temporaryOrderId = req.params.id;
            try {
                const temporaryOrder = await TemporaryOrder.findByPk(temporaryOrderId);
                if (!temporaryOrder) {
                    return res.status(404).json({ message: 'Temporary order not found' });
                }

                // find the ingredients used in the temporary order
                const menuItemIngredients = await MenuItemIngredient.findAll({
                    where: { menu_item_id: temporaryOrder.menu_item_id },
                    include: Ingredient
                });

                // restock the ingredients
                for (const item of menuItemIngredients) {
                    const ingredient = item.Ingredient;
                    const requiredQuantity = parseFloat((item.quantity_required * temporaryOrder.quantity).toFixed(2));
                    const updatedQuantity = parseFloat(ingredient.quantity) + requiredQuantity;
                    ingredient.quantity = parseFloat(updatedQuantity.toFixed(2));
                    await ingredient.save();
                }

                await temporaryOrder.destroy();
                res.json({ message: 'Temporary order deleted successfully' });
            } catch (error) {
                console.error('Error deleting temporary order:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        deleteTemporaryOrders: async (req, res) => {
            const temporaryCustomerId = req.params.customer_identifier;
            try {
                // find the temporary orders by customer identifier
                const temporaryOrders = await TemporaryOrder.findAll({ where: { customer_identifier: temporaryCustomerId } });

                // restock the ingredients for each temporary order
                for (const temporaryOrder of temporaryOrders) {
                    const menuItemIngredients = await MenuItemIngredient.findAll({
                        where: { menu_item_id: temporaryOrder.menu_item_id },
                        include: Ingredient
                    });

                    // restock the ingredients
                    for (const item of menuItemIngredients) {
                        const ingredient = item.Ingredient;
                        const requiredQuantity = parseFloat((item.quantity_required * temporaryOrder.quantity).toFixed(2));
                        const updatedQuantity = parseFloat(ingredient.quantity) + requiredQuantity;
                        ingredient.quantity = parseFloat(updatedQuantity.toFixed(2));
                        await ingredient.save();
                    }
                }

                // delete the temporary orders
                const deletedCount = await TemporaryOrder.destroy({ where: { customer_identifier: temporaryCustomerId } });
                if (deletedCount === 0) {
                    res.status(404).json({ message: 'No matching temporary orders found' });
                } else {
                    res.json({ message: `Deleted ${deletedCount} temporary orders successfully` });
                }
            } catch (error) {
                console.error('Error deleting temporary orders:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
        // get all temporary orders for the customer_identifier
        getAllTemporaryOrdersForCutomerIdentifier: async (req, res) => {
            try {
                const customerIdentifier = req.params.customer_identifier;
                const temporaryOrders = await TemporaryOrder.findAll({
                    include: {
                        model: MenuItem,
                        as: 'menuItem',
                        attributes: ['image_url', 'name']
                    },
                    where: {
                        customer_identifier: customerIdentifier,
                    }
                });

                res.json(temporaryOrders);
            } catch (error) {
                console.error('Error fetching temporary orders:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
