const { DataTypes } = require('sequelize');

module.exports = sequelize => {
    const Review = sequelize.define('Review', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        customer_name: {
            type: DataTypes.STRING,
        },
        rating: {
            type: DataTypes.INTEGER,
        },
        comment: {
            type: DataTypes.TEXT,
        },
        date: {
            type: DataTypes.DATE,
        },
    });

    return {
        // get all reviews
        getAllReviews: async (req, res) => {
            try {
                const reviews = await Review.findAll();
                res.json(reviews);
            } catch (error) {
                console.error('Error fetching reviews:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get review by ID
        getReviewById: async (req, res) => {
            const reviewId = req.params.id;
            try {
                const review = await Review.findByPk(reviewId);
                if (!review) {
                    res.status(404).json({ message: 'Review not found' });
                } else {
                    res.json(review);
                }
            } catch (error) {
                console.error('Error fetching review:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new review
        createReview: async (req, res) => {
            const { menu_item_id, customer_name, rating, comment, date } = req.body;
            try {
                const newReview = await Review.create({ menu_item_id, customer_name, rating, comment, date });
                res.status(201).json(newReview);
            } catch (error) {
                console.error('Error creating review:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update review by ID
        updateReview: async (req, res) => {
            const reviewId = req.params.id;
            const { menu_item_id, customer_name, rating, comment, date } = req.body;
            try {
                const review = await Review.findByPk(reviewId);
                if (!review) {
                    res.status(404).json({ message: 'Review not found' });
                } else {
                    await review.update({ menu_item_id, customer_name, rating, comment, date });
                    res.json(review);
                }
            } catch (error) {
                console.error('Error updating review:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // delete review by ID
        deleteReview: async (req, res) => {
            const reviewId = req.params.id;
            try {
                const review = await Review.findByPk(reviewId);
                if (!review) {
                    res.status(404).json({ message: 'Review not found' });
                } else {
                    await review.destroy();
                    res.json({ message: 'Review deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting review:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
