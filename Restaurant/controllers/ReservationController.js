const { DataTypes } = require('sequelize');

module.exports = sequelize => {
    const Reservation = sequelize.define('Reservation', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        customer_name: {
            type: DataTypes.STRING,
        },
        reservation_time: {
            type: DataTypes.DATE,
        },
        status: {
            type: DataTypes.ENUM('confirmed', 'pending', 'canceled'),
            defaultValue: 'pending',
        },
    });

    return {
        // get all reservations
        getAllReservations: async (req, res) => {
            try {
                const reservations = await Reservation.findAll();
                res.json(reservations);
            } catch (error) {
                console.error('Error fetching reservations:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get reservation by ID
        getReservationById: async (req, res) => {
            const reservationId = req.params.id;
            try {
                const reservation = await Reservation.findByPk(reservationId);
                if (!reservation) {
                    res.status(404).json({ message: 'Reservation not found' });
                } else {
                    res.json(reservation);
                }
            } catch (error) {
                console.error('Error fetching reservation:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new reservation
        createReservation: async (req, res) => {
            const { table_id, customer_name, reservation_time, status } = req.body;
            try {
                const newReservation = await Reservation.create({ table_id, customer_name, reservation_time, status });
                res.status(201).json(newReservation);
            } catch (error) {
                console.error('Error creating reservation:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update reservation by ID
        updateReservation: async (req, res) => {
            const reservationId = req.params.id;
            const { table_id, customer_name, reservation_time, status } = req.body;
            try {
                const reservation = await Reservation.findByPk(reservationId);
                if (!reservation) {
                    res.status(404).json({ message: 'Reservation not found' });
                } else {
                    await reservation.update({ table_id, customer_name, reservation_time, status });
                    res.json(reservation);
                }
            } catch (error) {
                console.error('Error updating reservation:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // delete reservation by ID
        deleteReservation: async (req, res) => {
            const reservationId = req.params.id;
            try {
                const reservation = await Reservation.findByPk(reservationId);
                if (!reservation) {
                    res.status(404).json({ message: 'Reservation not found' });
                } else {
                    await reservation.destroy();
                    res.json({ message: 'Reservation deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting reservation:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
