const bcrypt = require('bcrypt');
const { DataTypes } = require('sequelize');

module.exports = sequelize => {
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        username: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        role: {
            type: DataTypes.ENUM('waiter', 'kitchen', 'manager', 'admin-app-manager'),
            defaultValue: 'waiter',
        },
    });

    return {
        // get all users
        getAllUsers: async (req, res) => {
            try {
                const users = await User.findAll();
                res.json(users);
            } catch (error) {
                console.error('Error fetching users:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get user by ID
        getUserById: async (req, res) => {
            const userId = req.params.id;
            try {
                const user = await User.findByPk(userId);
                if (!user) {
                    res.status(404).json({ message: 'User not found' });
                } else {
                    res.json(user);
                }
            } catch (error) {
                console.error('Error fetching user:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        createUser: async (req, res) => {
            const { username, password, role } = req.body;
            try {
                const salt = await bcrypt.genSalt(10);
                const hashedPassword = await bcrypt.hash(password, salt);

                const newUser = await User.create({ username, password: hashedPassword, role });
                res.status(201).json(newUser);
            } catch (error) {
                console.error('Error creating user:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        updateUser: async (req, res) => {
            const userId = req.params.id;
            const { username, password, role } = req.body;
            try {
                const user = await User.findByPk(userId);
                if (!user) {
                    res.status(404).json({ message: 'User not found' });
                } else {
                    let newPassword = password;
                    if (password) { // only hash the password if it has changed
                        const salt = await bcrypt.genSalt(10);
                        newPassword = await bcrypt.hash(password, salt);
                    }

                    await user.update({ username, password: newPassword, role });
                    res.json(user);
                }
            } catch (error) {
                console.error('Error updating user:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },


        // delete user by ID
        deleteUser: async (req, res) => {
            const userId = req.params.id;
            try {
                const user = await User.findByPk(userId);
                if (!user) {
                    res.status(404).json({ message: 'User not found' });
                } else {
                    await user.destroy();
                    res.json({ message: 'User deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting user:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
