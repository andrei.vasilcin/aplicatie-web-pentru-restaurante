'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return [queryInterface.addColumn(
      'TemporaryOrders',
      'remark',
      Sequelize.TEXT
    ),
    queryInterface.addColumn(
      'Orders',
      'remark',
      Sequelize.TEXT
    )];
  },

  async down(queryInterface, Sequelize) {
    return [queryInterface.removeColumn(
      'TemporaryOrders',
      'remark',
    ),
    queryInterface.removeColumn(
      'Orders',
      'remark',
      Sequelize.TEXT
    )];
  }
};
