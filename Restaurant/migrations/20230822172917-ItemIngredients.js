'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return [queryInterface.addColumn(
      'MenuItemIngredients',
      'menu_item_id',
      Sequelize.INTEGER
    ),
    queryInterface.addColumn(
      'MenuItemIngredients',
      'ingredient_id',
      Sequelize.INTEGER
    )];
  },

  async down(queryInterface, Sequelize) {
    return [queryInterface.removeColumn(
      'MenuItemIngredients',
      'menu_item_id',
      Sequelize.INTEGER
    ),
    queryInterface.removeColumn(
      'MenuItemIngredients',
      'ingredient_id',
      Sequelize.INTEGER
    )];
  }
};
