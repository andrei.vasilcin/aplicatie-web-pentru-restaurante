const express = require('express');
const router = express.Router();


module.exports = sequelize => {
    const ReservationController = require('../controllers/ReservationController')(sequelize);
    // Define routes
    router.get('/', ReservationController.getAllReservations);
    router.get('/:id', ReservationController.getReservationById);
    router.post('/', ReservationController.createReservation);
    router.put('/:id', ReservationController.updateReservation);
    router.delete('/:id', ReservationController.deleteReservation);

    return router;
};