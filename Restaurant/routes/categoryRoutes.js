const express = require('express');
const router = express.Router();

// Define routes
module.exports = sequelize => {
    const CategoryController = require('../controllers/CategoryController')(sequelize);

    router.get('/', CategoryController.getAllCategories);
    router.get('/:id', CategoryController.getCategoryById);
    router.post('/', CategoryController.createCategory);
    router.put('/:id', CategoryController.updateCategory);
    router.delete('/:id', CategoryController.deleteCategory);

    return router;
};
