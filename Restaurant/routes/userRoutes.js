const express = require('express');
const { checkRole } = require('../middleware/authMiddleware');
const router = express.Router();



module.exports = sequelize => {
    const UserController = require('../controllers/UserController')(sequelize);
    router.get('/', UserController.getAllUsers);
    router.get('/:id', UserController.getUserById);
    router.post('/', UserController.createUser);
    router.put('/:id', UserController.updateUser);
    router.delete('/:id', UserController.deleteUser);

    return router;
};