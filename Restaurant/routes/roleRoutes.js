const express = require('express');
const router = express.Router();
const RoleController = require('../controllers/RoleController');

module.exports = sequelize => {
    const RoleController = require('../controllers/RoleController')(sequelize);
    // Define routes
    router.get('/', RoleController.getAllRoles);
    router.get('/:id', RoleController.getRoleById);
    router.post('/', RoleController.createRole);
    router.put('/:id', RoleController.updateRole);
    router.delete('/:id', RoleController.deleteRole);

    return router;
};