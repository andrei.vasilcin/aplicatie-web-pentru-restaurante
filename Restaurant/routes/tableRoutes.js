const express = require('express');
const router = express.Router();


module.exports = sequelize => {
    const TableController = require('../controllers/TableController')(sequelize);
    // Define routes
    router.get('/', TableController.getAllTables);
    router.get('/:id', TableController.getTableById);
    router.post('/', TableController.createTable);
    router.put('/:id', TableController.updateTable);
    router.delete('/:id', TableController.deleteTable);

    return router;
};