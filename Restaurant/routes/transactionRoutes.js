const express = require('express');
const router = express.Router();


module.exports = sequelize => {
    const TransactionController = require('../controllers/TransactionController')(sequelize);
    // Define routes
    router.get('/', TransactionController.getAllTransactions);
    router.get('/:id', TransactionController.getTransactionById);
    router.post('/', TransactionController.createTransaction);
    router.put('/:id', TransactionController.updateTransaction);
    router.delete('/:id', TransactionController.deleteTransaction);

    return router;
};