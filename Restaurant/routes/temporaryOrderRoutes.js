const express = require('express');
const router = express.Router();


module.exports = sequelize => {
    const TemporaryOrderController = require('../controllers/TemporaryOrderController')(sequelize);
    // Define routes
    router.get('/', TemporaryOrderController.getAllTemporaryOrders);
    router.get('/:id', TemporaryOrderController.getTemporaryOrderById);
    router.get('/customer_identifier/:customer_identifier', TemporaryOrderController.getAllTemporaryOrdersForCutomerIdentifier);
    router.post('/', TemporaryOrderController.createTemporaryOrder);
    router.post('/waiter', TemporaryOrderController.createTemporaryOrderWaiter);
    router.put('/:id', TemporaryOrderController.updateTemporaryOrder);
    router.delete('/:id', TemporaryOrderController.deleteTemporaryOrder);
    router.delete('/cancel/:customer_identifier', TemporaryOrderController.deleteTemporaryOrders);


    return router;
};