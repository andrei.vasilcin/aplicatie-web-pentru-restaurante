const JWT_SECRET = '8zxcvbcncmc4';


const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { Sequelize } = require('sequelize');
const multer = require('multer');
const path = require('path');

const sequelize = new Sequelize('restaurant_project', 'root', '', {
    host: 'localhost',
    port: 3307,
    dialect: 'mysql',
});

const Table = require('./models/Table');
const TemporaryOrder = require('./models/TemporaryOrder');
const Transaction = require('./models/Transaction');
const User = require('./models/User');
const Category = require('./models/Category')
const CustomersTable = require('./models/CustomersTable')
const Ingredient = require('./models/Ingredient');
const MenuItem = require('./models/MenuItem');
const MenuItemIngredient = require('./models/MenuItemIngredient');
const Order = require('./models/Order');
const Reservation = require('./models/Reservation')
const Review = require('./models/Review')
const Role = require('./models/Role')


const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/userRoutes')(sequelize);
const tableRoutes = require('./routes/tableRoutes')(sequelize);
const menuItemRoutes = require('./routes/menuItemRoutes')(sequelize);
const menuItemIngredientRoutes = require('./routes/menuItemIngredientRoutes')(sequelize);
const reservationRoutes = require('./routes/reservationRoutes')(sequelize);
const reviewRoutes = require('./routes/reviewRoutes')(sequelize);
const roleRoutes = require('./routes/roleRoutes')(sequelize);
const temporaryOrderRoutes = require('./routes/temporaryOrderRoutes')(sequelize);
const transactionRoutes = require('./routes/transactionRoutes')(sequelize);
const categoryRoutes = require('./routes/categoryRoutes')(sequelize);
const customerTableRoutes = require('./routes/customerTableRoutes')(sequelize);
const ingredientRoutes = require('./routes/ingredientRoutes')(sequelize);
const orderRoutes = require('./routes/orderRoutes')(sequelize);


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, '../the-project/public/assets/images');
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
const upload = multer({ storage: storage });


const app = express();

const server = http.createServer(app);

app.use(cors({
    origin: ["http://localhost:3000", "https://f2c0-2a02-2f09-330b-2700-6900-fd53-38f1-d36d.ngrok-free.app", "http://192.168.1.168:3000"],

    credentials: true
}));

const io = socketIO(server, {
    cors: {
        origin: ["http://localhost:3000", "https://f2c0-2a02-2f09-330b-2700-6900-fd53-38f1-d36d.ngrok-free.app", "http://192.168.1.168:3000"],
        methods: ["GET", "POST"],
        allowedHeaders: ["my-custom-header", "ngrok-skip-browser-warning"],
        credentials: true
    }
});


app.use((req, res, next) => {
    req.io = io;
    next();
});

const customHeadersAppLevel = function (req, res, next) {
    req.headers['ngrok-skip-browser-warning'] = 'true'
    next();
};

app.use(customHeadersAppLevel);


io.on('connection', (socket) => {


    socket.on('requestHelp', (helpRequest) => {
        // broadcast the help request to all other connected clients
        console.log("Sending help reques");
        socket.broadcast.emit('helpRequested', helpRequest);
    });
    console.log('Client connected: ' + socket.id);
    io.emit('testEvent', 'Hello from server!');
});
io.on('connect', () => {
    console.log('Client connected');

    //emit a welcome message
    io.emit('welcome', 'Hello from the server!');
});


// Define associations
Reservation.belongsTo(Table, { foreignKey: 'table_id' });
Table.hasMany(Reservation, { foreignKey: 'table_id' });
Table.hasMany(Order, { foreignKey: 'table_number' });

Category.hasMany(MenuItem, { foreignKey: 'category_id', sourceKey: 'id' });

MenuItem.belongsTo(Category, { foreignKey: 'category_id', targetKey: 'id' });
MenuItem.hasMany(MenuItemIngredient, { foreignKey: 'menu_item_id', as: 'ingredients' });
MenuItem.hasMany(Order, { foreignKey: 'menu_item_id' });

Order.belongsTo(MenuItem, { foreignKey: 'menu_item_id', as: 'menuItem' });
Order.belongsTo(Table, { foreignKey: 'table_number' });
Order.hasMany(TemporaryOrder, { foreignKey: 'customer_identifier' });

Ingredient.hasMany(MenuItemIngredient, { foreignKey: 'ingredient_id' });

MenuItemIngredient.belongsTo(MenuItem, { foreignKey: 'menu_item_id', as: 'menuItem' });
MenuItemIngredient.belongsTo(Ingredient, { foreignKey: 'ingredient_id' });

Role.hasMany(User, { foreignKey: 'role' });
User.hasOne(Role, { foreignKey: 'role', sourceKey: 'id' });

Review.belongsTo(MenuItem, { foreignKey: 'menu_item_id' });

TemporaryOrder.belongsTo(Table, { foreignKey: 'table_number' });
TemporaryOrder.belongsTo(MenuItem, { foreignKey: 'menu_item_id', as: 'menuItem' });

Transaction.belongsTo(User, { foreignKey: 'user_id' });



app.use(bodyParser.json());
app.use(express.json());




app.get('/', (req, res) => {
    res.send('API is running');
});





app.use('/auth', authRoutes);
app.use('/users', userRoutes);
app.use('/tables', tableRoutes);
app.use('/menu-items', upload.single('image_url'), menuItemRoutes);


app.use('/menu-item-ingredients', menuItemIngredientRoutes);
app.use('/reservations', reservationRoutes);
app.use('/reviews', reviewRoutes);
app.use('/roles', roleRoutes);
app.use('/temporary-orders', temporaryOrderRoutes);
app.use('/transactions', transactionRoutes);
app.use('/categories', categoryRoutes);
app.use('/customer-tables', customerTableRoutes);
app.use('/ingredients', ingredientRoutes);
app.use('/orders', orderRoutes);



const verifyToken = (req, res, next) => {
    const token = req.headers.authorization;

    if (!token) {
        return res.status(401).json({ message: 'Token missing' });
    }

    jwt.verify(token, JWT_SECRET, (err, decoded) => {
        if (err) {
            return res.status(401).json({ message: 'Invalid token' });
        }

        req.userId = decoded.userId;
        next();
    });
};


app.get('/test-db', async (req, res) => {
    try {
        await sequelize.authenticate();
        res.send('Database connection successful.');
    } catch (error) {
        console.error('Error connecting to the database:', error);
        res.status(500).send('Database connection failed.');
    }
});



app.get('/protected', verifyToken, (req, res) => {

    res.json({ message: 'Protected route', userId: req.userId });
});


app.get('/profile', verifyToken, (req, res) => {

    res.json({ message: 'User profile route', userId: req.userId });
});

app.use('/assets/images', express.static('assets/images'));


sequelize.sync();

server.listen(5001, () => {
    console.log('Server running on http://localhost:5001/');
});

module.exports = { io };